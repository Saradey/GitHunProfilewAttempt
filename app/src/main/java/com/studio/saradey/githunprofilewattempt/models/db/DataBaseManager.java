package com.studio.saradey.githunprofilewattempt.models.db;


import android.support.annotation.NonNull;

import com.studio.saradey.githunprofilewattempt.views.adapters.UserView;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;


/**
 * @author Goncharov Evgeny on 11.07.18
 */


public class DataBaseManager {

    private Realm realm;

    private RealmResults<UserDateBase> record;


    /////////////////////////////////////////////////////
    // If press to item recycle view than update item
    ////////////////////////////////////////////////////
    public void pressList(String id) {
        realm = Realm.getDefaultInstance();
        realm.executeTransaction(getDoItPressToTrue(id));
        realm.close();
    }


    @NonNull
    private Realm.Transaction getDoItPressToTrue(String id) {
        return data -> {
            UserDateBase userUpdate = realm.where(UserDateBase.class).equalTo("id", id).findFirst();
            userUpdate.setPress(true);
        };
    }


    /////////////////////////////////////////////////////
    // Set list to db
    ////////////////////////////////////////////////////
    public void setListToData(List<UserDateBase> record) {
        realm = Realm.getDefaultInstance();
        realm.executeTransaction(getTransactionSetData(record));
        realm.close();
    }



    /////////////////////////////////////////////////////
    // Getters to setListToData
    ////////////////////////////////////////////////////
    //-----Begin-----------------------------------------
    @NonNull
    private Realm.Transaction getTransactionSetData(List<UserDateBase> record) {
        return data -> {
            for (UserDateBase it : record)
                data.insertOrUpdate(it);
        };
    }
    //-----End-----------------------------------------


    /////////////////////////////////////////////////////
    // Load from db and go one
    ////////////////////////////////////////////////////
    public List<UserView> getListUsers() {
        realm = Realm.getDefaultInstance();

        realm.executeTransaction(data -> {
            record = data.where(UserDateBase.class).findAll();
        });

        List<UserView> userRecord = new ArrayList<>();
        for (UserDateBase it : record) {
            UserView user = new UserView();
            user.setLogin(it.getLogin());
            user.setId(it.getId());
            user.setUrl(it.getAvatar_url());
            userRecord.add(user);
        }

        realm.close();
        return userRecord;
    }


    /////////////////////////////////////////////////////
    // Get login isPress = true
    ////////////////////////////////////////////////////
    public String getLoginUser() {
        String login;

        realm = Realm.getDefaultInstance();
        UserDateBase userDateBase = realm.where(UserDateBase.class).
                equalTo("isPress", true).findFirst();
        login = userDateBase.getLogin();
        realm.close();

        return login;
    }


    /////////////////////////////////////////////////////
    // Set User to database
    ////////////////////////////////////////////////////
    public void setUserToData(UserDateBase user) {
        realm = Realm.getDefaultInstance();

        realm.executeTransaction(data -> {
            data.insertOrUpdate(user);
        });

        realm.close();
    }


    /////////////////////////////////////////////////////
    // Get One User to PresenterUser
    ////////////////////////////////////////////////////
    public UserView getOneUser() {
        realm = Realm.getDefaultInstance();
        UserDateBase oneUser = realm.where(UserDateBase.class).
                equalTo("isPress", true).findFirst();

        UserView userView = new UserView();
        userView.setBio(oneUser.getBio());
        userView.setCompany(oneUser.getCompany());
        userView.setCreated_at(oneUser.getCreate_at());
        userView.setId(oneUser.getId());
        userView.setLocation(oneUser.getLocation());
        userView.setLogin(oneUser.getLogin());
        userView.setType(oneUser.getType());
        userView.setUrl(oneUser.getAvatar_url());

        realm.executeTransaction(getTransactionUpdateUser());
        realm.close();

        return userView;
    }

    /////////////////////////////////////////////////////
    // Getters to getOneUser()
    ////////////////////////////////////////////////////
    //-----Begin-----------------------------------------
    @NonNull
    private Realm.Transaction getTransactionUpdateUser() {
        return data -> {
            RealmResults<UserDateBase> list = realm.where(UserDateBase.class).findAll();

            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getPress() == true) {
                    list.get(i).setPress(false);
                }
            }

        };
    }
    //-----End-----------------------------------------

}
