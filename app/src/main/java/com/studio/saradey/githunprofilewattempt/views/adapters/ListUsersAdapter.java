package com.studio.saradey.githunprofilewattempt.views.adapters;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.studio.saradey.githunprofilewattempt.R;
import com.studio.saradey.githunprofilewattempt.views.fragments.ListUsersFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListUsersAdapter extends RecyclerView.Adapter<ListUsersAdapter.ListHolder> {


    private static final String TAG = "GitHub ListUsersAdapter";

    private static RecyclerViewClickListener itemListener;

    private static List<UserView> record;


    public ListUsersAdapter(List<UserView> record, RecyclerViewClickListener itemListener) {
        this.record = record;
        ListUsersAdapter.itemListener = itemListener;
    }


    /////////////////////////////////////////////////////
    // interface listener for fragment
    ////////////////////////////////////////////////////
    public interface RecyclerViewClickListener {
        void pressToList(String login);
    }




    @NonNull
    @Override
    public ListHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycle_user_list, viewGroup, false);

        return new ListHolder(v);
    }



    @Override
    public void onBindViewHolder(@NonNull ListHolder holder, int position) {
        holder.bind((UserView) record.get(position));
    }


    /////////////////////////////////////////////////////
    // For show
    ////////////////////////////////////////////////////
    private void showLog(String show) {
        Log.d(TAG, show);
    }


    @Override
    public int getItemCount() {
        return record.size();
    }


    /////////////////////////////////////////////////////
    // holder this adapter
    ////////////////////////////////////////////////////
    public static class ListHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.user_avatar)
        ImageView userAvatar;

        @BindView(R.id.info_name)
        TextView info_name;

        @BindView(R.id.info_id)
        TextView info_id;

        ListHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
            v.setOnClickListener(getClick());
        }


        //TODO исправить
        @SuppressLint("SetTextI18n")
        void bind(UserView user) {
            info_name.setText("Login:" + user.getLogin());
            info_id.setText("Id:" + user.getId());

            if (user.getUrl() != null) {
                Glide.with(((ListUsersFragment) itemListener).getActivity())
                        .load(user.getUrl())
                        .into(userAvatar);
            }
        }

        /////////////////////////////////////////////////////
        // if click to item
        ////////////////////////////////////////////////////
        @NonNull
        private View.OnClickListener getClick() {
            return new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemListener.pressToList(record.get(getLayoutPosition()).getId());
                }
            };
        }


    }


}
