package com.studio.saradey.githunprofilewattempt.main;


import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;


public class MainApp extends Application {


    /////////////////////////////////////////////////////
    // Main onCreate, init Realm
    ////////////////////////////////////////////////////
    @Override
    public void onCreate() {
        super.onCreate();

        //TODO возможно сделать инъекцию
        Realm.init(this);
        RealmConfiguration configuration = new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(configuration);
    }

}
