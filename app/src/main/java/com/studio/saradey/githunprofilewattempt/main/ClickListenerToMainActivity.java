package com.studio.saradey.githunprofilewattempt.main;

/**
 * @author jtgn on 12.07.2018
 */
public interface ClickListenerToMainActivity {

    //action from ListUsersFragment
    void goToTheUserFragment();

}
