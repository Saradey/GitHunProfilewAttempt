package com.studio.saradey.githunprofilewattempt.models.db;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;



/////////////////////////////////////////////////////
// Class for pars json and write to db
////////////////////////////////////////////////////
public class UserDateBase extends RealmObject {

    @SerializedName("avatar_url")
    @Expose
    private String avatar_url;

    @SerializedName("login")
    @Expose
    private String login;

    @SerializedName("id")
    @Expose
    @PrimaryKey
    private String id;

    @SerializedName("company")
    @Expose
    private String company;

    @SerializedName("location")
    @Expose
    private String location;

    @SerializedName("created_at")
    @Expose
    private String create_at;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("bio")
    @Expose
    private String bio;

    private boolean isPress;


    /////////////////////////////////////////////////////
    // Getters and Setters
    ////////////////////////////////////////////////////
    //-----Begin-----------------------------------------
    public boolean getPress() {
        return isPress;
    }

    public void setPress(boolean press) {
        isPress = press;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCreate_at() {
        return create_at;
    }

    public void setCreate_at(String create_at) {
        this.create_at = create_at;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }
    //-----End-----------------------------------------
}
