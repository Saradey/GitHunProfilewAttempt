package com.studio.saradey.githunprofilewattempt.views.fragments;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.bumptech.glide.Glide;
import com.studio.saradey.githunprofilewattempt.R;
import com.studio.saradey.githunprofilewattempt.models.net.Caller;
import com.studio.saradey.githunprofilewattempt.presenters.PresenterUser;
import com.studio.saradey.githunprofilewattempt.presenters.UserPresenter;
import com.studio.saradey.githunprofilewattempt.views.adapters.UserView;

import butterknife.BindView;
import butterknife.ButterKnife;


public class UserInformationFragment extends MvpAppCompatFragment implements UserPresenter {

    private static final String TAG = "GitHub UserInformationFragment";

    @InjectPresenter
    PresenterUser presenterUser;

    @BindView(R.id.ava)
    ImageView avatar;

    @BindView(R.id.name_user)
    TextView name;

    @BindView(R.id.id_user)
    TextView id;

    @BindView(R.id.company_viewText)
    TextView company;

    @BindView(R.id.location_viewText)
    TextView location;

    @BindView(R.id.created_at_viewText)
    TextView createdAt;

    @BindView(R.id.type_view_text)
    TextView type;

    @BindView(R.id.bio_view_text)
    TextView bio;


    /////////////////////////////////////////////////////
    // Create
    ////////////////////////////////////////////////////
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_user, container, false);

        ButterKnife.bind(this, v);
        initUserToView(v);
        return v;
    }


    /////////////////////////////////////////////////////
    // Attach to presenter and load user
    ////////////////////////////////////////////////////
    private void initUserToView(View v) {
        presenterUser.attachView(this);
        presenterUser.loadUser(v);
    }


    /////////////////////////////////////////////////////
    // Set data to view
    ////////////////////////////////////////////////////
    @Override
    public void setUserViewToView(UserView userView) {
        String descript_name = getString(R.string.to_name) + userView.getLogin();
        name.setText(descript_name);
        String descript_id = getString(R.string.to_id) + userView.getId();
        id.setText(descript_id);
        String descript_Company = getString(R.string.to_company) + userView.getCompany();
        company.setText(descript_Company);
        String descript_Location = getString(R.string.to_location) + userView.getLocation();
        location.setText(descript_Location);
        String descript_createAt = getString(R.string.to_create_at) + userView.getCreated_at();
        createdAt.setText(descript_createAt);
        String descript_type = getString(R.string.to_type) + userView.getType();
        type.setText(descript_type);
        String descript_bio = getString(R.string.to_bio) + userView.getBio();
        bio.setText(descript_bio);

        Glide.with(getActivity())
                .load(userView.getUrl())
                .into(avatar);
    }


    /////////////////////////////////////////////////////
    // For the show exception
    ////////////////////////////////////////////////////
    @Override
    public void showError(int code) {
        switch (code) {
            case Caller.NO_CONNECTED:
                showToast(getString(R.string.error_internet));
                break;
        }
    }


    /////////////////////////////////////////////////////
    // For test
    ////////////////////////////////////////////////////
    @SuppressLint("LongLogTag")
    private void showLog(String show) {
        Log.d(TAG, show);
    }

    private void showToast(String show) {
        Toast toast = Toast.makeText(getActivity(),
                show, Toast.LENGTH_SHORT);
        toast.show();
    }

}
