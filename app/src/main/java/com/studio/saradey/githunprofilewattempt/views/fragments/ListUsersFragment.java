package com.studio.saradey.githunprofilewattempt.views.fragments;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.studio.saradey.githunprofilewattempt.R;
import com.studio.saradey.githunprofilewattempt.main.ClickListenerToMainActivity;
import com.studio.saradey.githunprofilewattempt.models.net.Caller;
import com.studio.saradey.githunprofilewattempt.presenters.ListUsers;
import com.studio.saradey.githunprofilewattempt.presenters.PresenterListUsers;
import com.studio.saradey.githunprofilewattempt.views.adapters.ListUsersAdapter;
import com.studio.saradey.githunprofilewattempt.views.adapters.UserView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ListUsersFragment extends MvpAppCompatFragment implements
        ListUsers,
        ListUsersAdapter.RecyclerViewClickListener {

    private static final int VERTICAL = 1;
    private static final String TAG = "GitHub ListUsersFragment";

    @BindView(R.id.list_user)
    RecyclerView recyclerView;

    @InjectPresenter
    PresenterListUsers presenterListStory;

    private ListUsersAdapter adapter;

    private ClickListenerToMainActivity listenerActivity;


    /////////////////////////////////////////////////////
    // create this view
    ////////////////////////////////////////////////////
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_fragment_list_users, container, false);

        ButterKnife.bind(this, v);
        loadList();

        return v;
    }


    /////////////////////////////////////////////////////
    // check for internet and say about it to Presenter
    ////////////////////////////////////////////////////
    private void loadList() {
        presenterListStory.attachView(this);
        presenterListStory.loadList();
    }


    /////////////////////////////////////////////////////
    // get list and set to recycle
    ////////////////////////////////////////////////////
    @Override
    public void initRecycleView(List<UserView> record) {
        adapter = new ListUsersAdapter(record, this);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        gridLayoutManager.setOrientation(VERTICAL);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(adapter);
    }


    /////////////////////////////////////////////////////
    // if press to item recycle
    ////////////////////////////////////////////////////
    @Override
    public void pressToList(String id) {
        presenterListStory.ifPressThenDataBaseUpdate(id);
        listenerActivity.goToTheUserFragment();
    }


    /////////////////////////////////////////////////////
    // Setters and Getters
    ////////////////////////////////////////////////////
    //-----Begin-----------------------------------------
    public void setListenerActivity(ClickListenerToMainActivity listenerActivity) {
        this.listenerActivity = listenerActivity;
    }
    //-----End-----------------------------------------


    /////////////////////////////////////////////////////
    // For the show exception
    ////////////////////////////////////////////////////
    @Override
    public void showError(int code) {

        switch (code) {
            case Caller.NO_CONNECTED:
                showToast(getString(R.string.error_internet));
                break;

        }

    }


    /////////////////////////////////////////////////////
    // for test
    ////////////////////////////////////////////////////
    @SuppressLint("LongLogTag")
    private void showLog(String show) {
        Log.d(TAG, show);
    }

    private void showToast(String show) {
        Toast toast = Toast.makeText(getActivity(),
                show, Toast.LENGTH_SHORT);
        toast.show();
    }


}
