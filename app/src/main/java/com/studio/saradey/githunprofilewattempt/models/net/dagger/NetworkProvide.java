package com.studio.saradey.githunprofilewattempt.models.net.dagger;


import android.content.Context;
import android.net.ConnectivityManager;

import com.studio.saradey.githunprofilewattempt.main.ContextProvider;

import dagger.Module;
import dagger.Provides;

@Module(includes = ContextProvider.class)
public class NetworkProvide {


    /////////////////////////////////////////////////////
    // inject to Caller for inspection
    ////////////////////////////////////////////////////
    @Provides
    public ConnectivityManager getNetworkInfo(Context context) {
        ConnectivityManager connect = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connect;
    }


}
