package com.studio.saradey.githunprofilewattempt.presenters;


import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.studio.saradey.githunprofilewattempt.main.ContextProvider;
import com.studio.saradey.githunprofilewattempt.models.db.DataBaseManager;
import com.studio.saradey.githunprofilewattempt.models.db.UserDateBase;
import com.studio.saradey.githunprofilewattempt.models.net.Caller;
import com.studio.saradey.githunprofilewattempt.presenters.dagger.DaggerInjectToPresenter;
import com.studio.saradey.githunprofilewattempt.views.adapters.UserView;
import com.studio.saradey.githunprofilewattempt.views.fragments.UserInformationFragment;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class PresenterUser extends MvpPresenter<UserPresenter> {


    private static final String TAG = "GitHub PresenterUser";

    @Inject
    DataBaseManager manager;

    @Inject
    Caller caller;


    @Override
    public void attachView(UserPresenter view) {
        DaggerInjectToPresenter.builder()
                .contextProvider(new ContextProvider(((UserInformationFragment) view).getContext()))
                .build()
                .injectToPresenterUser(this);

        super.attachView(view);
    }


    /////////////////////////////////////////////////////
    // Load user from internet or db
    ////////////////////////////////////////////////////
    public void loadUser(View context) {
        if (caller.isConnected()) {
            getUserForInternet(caller);
        } else {
            getViewState().showError(Caller.NO_CONNECTED);
            getUserForDatabase();
        }
    }


    /////////////////////////////////////////////////////
    // Load user from internet
    ////////////////////////////////////////////////////
    private void getUserForInternet(Caller caller) {
        Flowable flowable = caller.loadUserFromGit(manager.getLoginUser());
        flowable.observeOn(AndroidSchedulers.mainThread())
                .subscribe(getConsumer());
    }


    /////////////////////////////////////////////////////
    // Getters to getUserForInternet
    ////////////////////////////////////////////////////
    //-----Begin-----------------------------------------
    @NonNull
    private Consumer getConsumer() {
        return emmitar -> {
            List<UserDateBase> user = (List<UserDateBase>) emmitar;
            UserView userView = new UserView();

            UserDateBase userData = null;
            try {
                userData = user.get(0);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (userData != null) {
                userView.setBio(userData.getBio());
                userView.setCompany(userData.getCompany());
                userView.setCreated_at(userData.getCreate_at());
                userView.setId(userData.getId());
                userView.setLocation(userData.getLocation());
                userView.setLogin(userData.getLogin());
                userView.setType(userData.getType());
                userView.setUrl(userData.getAvatar_url());

                PresenterUser.this.setUserViewToView(userView);
                PresenterUser.this.setUserToDataBase(userData);
            }
        };
    }
    //-----End-----------------------------------------


    /////////////////////////////////////////////////////
    // Save user from db
    ////////////////////////////////////////////////////
    private void setUserToDataBase(UserDateBase user) {
        manager.setUserToData(user);
    }


    /////////////////////////////////////////////////////
    // Set data for show
    ////////////////////////////////////////////////////
    private void setUserViewToView(UserView userView) {
        getViewState().setUserViewToView(userView);
    }


    /////////////////////////////////////////////////////
    // Get user from database and put to adapter
    ////////////////////////////////////////////////////
    private void getUserForDatabase() {
        Flowable.fromCallable(getCallableLoadDb())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getSubSetToView());
    }


    /////////////////////////////////////////////////////
    // Getters to getUserForDatabase
    ////////////////////////////////////////////////////
    //-----Begin-----------------------------------------
    @NonNull
    private Callable<UserView> getCallableLoadDb() {
        return () -> {
            UserView userView = manager.getOneUser();
            return userView;
        };
    }

    @NonNull
    private Subscriber<UserView> getSubSetToView() {
        return new Subscriber<UserView>() {
            @Override
            public void onSubscribe(Subscription s) {
                s.request(Long.MAX_VALUE);
            }

            @Override
            public void onNext(UserView userView) {
                PresenterUser.this.setUserViewToView(userView);
            }

            @Override
            public void onError(Throwable t) {
                showLog("Error set to view");
                t.printStackTrace();
            }

            @Override
            public void onComplete() {
                showLog("onComplete set to view");
            }
        };
    }

    //-----End-----------------------------------------


    /////////////////////////////////////////////////////
    // For show
    ////////////////////////////////////////////////////
    private void showLog(String show) {
        Log.d(TAG, show);
    }

}
