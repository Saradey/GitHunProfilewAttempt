package com.studio.saradey.githunprofilewattempt.presenters.dagger;

import android.content.Context;

import com.studio.saradey.githunprofilewattempt.main.ContextProvider;
import com.studio.saradey.githunprofilewattempt.models.db.DataBaseManager;
import com.studio.saradey.githunprofilewattempt.models.net.Caller;

import dagger.Module;
import dagger.Provides;

/**
 * @author Evgeny Goncharov on 13.07.2018
 */


@Module(includes = ContextProvider.class)
public class DaggerPresenterInjectModule {


    /////////////////////////////////////////////////////
    // inject to all Presenter, set DataBaseManager
    ////////////////////////////////////////////////////
    @Provides
    DataBaseManager getDataBaseManager() {
        DataBaseManager dataBaseManager = new DataBaseManager();
        return dataBaseManager;
    }


    /////////////////////////////////////////////////////
    // inject to all Presenter, set Caller
    ////////////////////////////////////////////////////
    @Provides
    Caller getCaller(Context context) {
        Caller caller = new Caller(context);
        return caller;
    }


}
