package com.studio.saradey.githunprofilewattempt.models.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.util.Log;

import com.studio.saradey.githunprofilewattempt.main.ContextProvider;
import com.studio.saradey.githunprofilewattempt.models.db.UserDateBase;
import com.studio.saradey.githunprofilewattempt.models.net.dagger.DaggerIngectorToCaller;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * @author Goncharov Evgeny on 11.07.18
 */

public class Caller {

    public final static int ALL_GUT = 200;

    @Inject
    ConnectivityManager manager;

    @Inject
    Endpoints endpoints;
    public final static int NO_CONNECTED = 1;
    private static final String TAG = "GitHub Caller";
    private List<UserDateBase> record = new ArrayList<>();
    private Call<List<UserDateBase>> call;



    /////////////////////////////////////////////////////
    // this constructor
    ////////////////////////////////////////////////////
    public Caller(Context context) {
        DaggerIngectorToCaller.builder()
                .contextProvider(new ContextProvider(context))
                .build()
                .injectToCaller(this);
    }

    /////////////////////////////////////////////////////
    // Load list from Internet
    ////////////////////////////////////////////////////
    public Flowable loadListUsersFromGit() {
        call = endpoints.loadUsers();  //здесь мы получаем наш call

        Flowable flowable = Flowable.just(call)
                .map(getLambdaLoadUserList())
                .subscribeOn(Schedulers.io())
                .delay(1000, TimeUnit.MILLISECONDS);

        return flowable;
    }


    /////////////////////////////////////////////////////
    // Getters to loadListUsersFromGit()
    ////////////////////////////////////////////////////
    //-----Begin-----------------------------------------
    @NonNull
    private Function<Call<List<UserDateBase>>, Object> getLambdaLoadUserList() {
        return emmitar -> {
            emmitar.enqueue(new Callback<List<UserDateBase>>() {
                @Override
                public void
                onResponse(@NonNull Call<List<UserDateBase>> call,
                           @NonNull Response<List<UserDateBase>> response) {
                    if (response.isSuccessful()) {

                        if (response.body() != null) {
                            for (int i = 0; i < response.body().size(); i++) {
                                UserDateBase user = response.body().get(i);
                                record.add(user);
                            }
                        } else {
                            showLog("not loading data users list");
                        }

                    } else {
                        showLog("response not successful users list " + response.code());
                    }

                }

                @Override
                public void onFailure(Call<List<UserDateBase>> call, Throwable t) {
                    showLog("onFailure from Callback users list");
                    t.printStackTrace();
                }

            });
            return record;
        };
    }
    //-----end-----------------------------------------


    /////////////////////////////////////////////////////
    // Load user from internet
    ////////////////////////////////////////////////////
    public Flowable loadUserFromGit(String login) {
        Call<UserDateBase> call = endpoints.getUser(login);  //здесь мы получаем наш call

        Flowable flowable = Flowable.just(call)
                .map(getCallUserDateBaseFunction())
                .subscribeOn(Schedulers.io())
                .delay(1000, TimeUnit.MILLISECONDS);

        return flowable;
    }


    @NonNull
    private Function<Call<UserDateBase>, List<UserDateBase>> getCallUserDateBaseFunction() {
        return emmitar -> {
            record = new ArrayList<>();

            emmitar.enqueue(new Callback<UserDateBase>() {
                @Override
                public void onResponse(@NonNull Call<UserDateBase> call,
                                       @NonNull Response<UserDateBase> response) {

                    if (response.isSuccessful()) {

                        if (response.body() != null) {
                            record.add(response.body());
                        } else {
                            showLog("not loading data user");
                        }

                    } else {
                        showLog("response not successful user " + response.code());
                    }

                }

                @Override
                public void onFailure(Call<UserDateBase> call, Throwable t) {
                    showLog("onFailure from Callback user");
                    t.printStackTrace();
                }

            });

            return record;
        };
    }




    /////////////////////////////////////////////////////
    // inspect NetworkInfo Internet off or on?
    ////////////////////////////////////////////////////
    public boolean isConnected() {
        NetworkInfo network = manager.getActiveNetworkInfo();
        return (network != null && network.isConnected());
    }


    ////////////////////////////////////////////////////
    // For show
    ////////////////////////////////////////////////////
    private void showLog(String show) {
        Log.d(TAG, show);
    }

}
