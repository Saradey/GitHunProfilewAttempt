package com.studio.saradey.githunprofilewattempt.models.net.dagger;


import com.studio.saradey.githunprofilewattempt.models.net.Caller;

import dagger.Component;

@Component(modules = {DaggerNetModule.class, NetworkProvide.class})
public interface IngectorToCaller {

    //inject Caller
    void injectToCaller(Caller caller);

}
