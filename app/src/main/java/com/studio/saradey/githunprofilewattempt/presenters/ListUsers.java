package com.studio.saradey.githunprofilewattempt.presenters;

import com.arellomobile.mvp.MvpView;
import com.studio.saradey.githunprofilewattempt.views.adapters.UserView;

import java.util.List;


public interface ListUsers extends MvpView {

    //initialization adapter later recycle
    void initRecycleView(List<UserView> record);

    //if exception
    void showError(int code);

}
