package com.studio.saradey.githunprofilewattempt.models.net.dagger;


import com.studio.saradey.githunprofilewattempt.models.net.Endpoints;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class DaggerNetModule {


    @Provides
    Retrofit getRetrofit() {
        return new Retrofit.Builder()
                .baseUrl("https://api.github.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }



    @Provides
    Endpoints getEndpoints(Retrofit retrofit) {
        Endpoints restAPI = retrofit.create(Endpoints.class);
        return restAPI;
    }


}
