package com.studio.saradey.githunprofilewattempt.main;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.studio.saradey.githunprofilewattempt.R;
import com.studio.saradey.githunprofilewattempt.views.fragments.ListUsersFragment;
import com.studio.saradey.githunprofilewattempt.views.fragments.UserInformationFragment;


public class MainActivity extends AppCompatActivity implements ClickListenerToMainActivity {


    private static final String TAG = "GitHub MainActivity";


    /////////////////////////////////////////////////////
    // Method onCreate
    ////////////////////////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initListUsersFragment();
    }


    /////////////////////////////////////////////////////
    // Initialize ListUsersFragment
    ////////////////////////////////////////////////////
    private void initListUsersFragment() {
        ListUsersFragment listUsers = new ListUsersFragment();
        listUsers.setListenerActivity(this);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.addToBackStack(null)
                .replace(R.id.space_fragment, listUsers)
                .commit();
    }



    /////////////////////////////////////////////////////
    // Transition logic between fragments
    ////////////////////////////////////////////////////
    //-----Begin-----------------------------------------
    public void goToTheUserFragment() {
        UserInformationFragment fragmentUsers = new UserInformationFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.addToBackStack(null)
                .replace(R.id.space_fragment, fragmentUsers)
                .commit();
    }
    //-----end-----------------------------------------


    /////////////////////////////////////////////////////
    // Button back on the phone
    ////////////////////////////////////////////////////
    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        }

        initListUsersFragment();
    }




    /////////////////////////////////////////////////////
    // For the test
    ////////////////////////////////////////////////////
    private void showLog(String show) {
        Log.d(TAG, show);
    }

    private void showToast(String show) {
        Toast toast = Toast.makeText(getApplicationContext(),
                show, Toast.LENGTH_SHORT);
        toast.show();
    }


}
