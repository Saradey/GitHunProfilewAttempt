package com.studio.saradey.githunprofilewattempt.presenters.dagger;

import com.studio.saradey.githunprofilewattempt.presenters.PresenterListUsers;
import com.studio.saradey.githunprofilewattempt.presenters.PresenterUser;

import dagger.Component;

/**
 * @author Evgeny Goncharov on 13.07.2018
 */


@Component(modules = {DaggerPresenterInjectModule.class})
public interface InjectToPresenter {

    void injectToPresenterListUsers(PresenterListUsers presenter);

    void injectToPresenterUser(PresenterUser presenter);

}
