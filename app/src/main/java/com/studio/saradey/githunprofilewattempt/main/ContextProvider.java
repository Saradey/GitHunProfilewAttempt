package com.studio.saradey.githunprofilewattempt.main;


import android.content.Context;

import dagger.Module;
import dagger.Provides;


/////////////////////////////////////////////////////
// includes for NetworkProvide
////////////////////////////////////////////////////
@Module
public class ContextProvider {
    Context context;

    public ContextProvider(Context context) {
        this.context = context;
    }


    @Provides
    public Context getContext() {
        return context.getApplicationContext();
    }
}