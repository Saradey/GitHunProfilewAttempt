package com.studio.saradey.githunprofilewattempt.presenters;

import com.arellomobile.mvp.MvpView;
import com.studio.saradey.githunprofilewattempt.views.adapters.UserView;

public interface UserPresenter extends MvpView {

    //Set data to view
    void setUserViewToView(UserView userView);

    //show error
    void showError(int code);

}
