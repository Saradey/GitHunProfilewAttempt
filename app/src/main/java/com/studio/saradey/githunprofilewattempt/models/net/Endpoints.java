package com.studio.saradey.githunprofilewattempt.models.net;

import com.studio.saradey.githunprofilewattempt.models.db.UserDateBase;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface Endpoints {

    //Api load list users from GitHub
    @GET("users")
    Call<List<UserDateBase>> loadUsers();


    //Api load users from GitHub
    @GET("/users/{user}")
    Call<UserDateBase> getUser(@Path("user") String user);

}
