package com.studio.saradey.githunprofilewattempt.presenters;


import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.studio.saradey.githunprofilewattempt.main.ContextProvider;
import com.studio.saradey.githunprofilewattempt.models.db.DataBaseManager;
import com.studio.saradey.githunprofilewattempt.models.db.UserDateBase;
import com.studio.saradey.githunprofilewattempt.models.net.Caller;
import com.studio.saradey.githunprofilewattempt.presenters.dagger.DaggerInjectToPresenter;
import com.studio.saradey.githunprofilewattempt.views.adapters.UserView;
import com.studio.saradey.githunprofilewattempt.views.fragments.ListUsersFragment;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;


@InjectViewState
public class PresenterListUsers extends MvpPresenter<ListUsers> {

    private static final String TAG = "GitHub PresenterListUsers";

    @Inject
    DataBaseManager manager;

    @Inject
    Caller caller;


    @Override
    public void attachView(ListUsers view) {
        DaggerInjectToPresenter.builder()
                .contextProvider(new ContextProvider(((ListUsersFragment) view).getContext()))
                .build()
                .injectToPresenterListUsers(this);

        super.attachView(view);
    }


    /////////////////////////////////////////////////////
    // Load list from internet or database
    ////////////////////////////////////////////////////
    public void loadList() {
        if (caller.isConnected()) {
            getListForInternet(caller);
        } else {
            getViewState().showError(Caller.NO_CONNECTED);
            getListForDataBase();
        }
    }


    /////////////////////////////////////////////////////
    // Load list from internet
    ////////////////////////////////////////////////////
    private void getListForInternet(Caller caller) {
        Flowable flowable = caller.loadListUsersFromGit();
        flowable.observeOn(AndroidSchedulers.mainThread())
                .subscribe(getLambdaPutListUserForAdapter());
    }


    /////////////////////////////////////////////////////
    // Getters to getListForInternet()
    ////////////////////////////////////////////////////
    //-----Begin-----------------------------------------
    @NonNull
    private Consumer getLambdaPutListUserForAdapter() {
        return emmitar -> {
            List<UserView> record = new ArrayList<>();
            for (UserDateBase it : (List<UserDateBase>) emmitar) {
                UserView userView = new UserView();
                userView.setId(it.getId());
                userView.setLogin(it.getLogin());
                userView.setUrl(it.getAvatar_url());
                record.add(userView);
            }

            PresenterListUsers.this.setToRecicleViewElement(record);
            PresenterListUsers.this.setListToDataBase((List<UserDateBase>) emmitar);
        };
    }


    private void setListToDataBase(List<UserDateBase> listDataBase) {
        manager.setListToData(listDataBase);
    }
    //-----end-----------------------------------------


    /////////////////////////////////////////////////////
    // get list from database
    ////////////////////////////////////////////////////
    private void getListForDataBase() {
        Flowable.fromCallable(getListCallable())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getSubFromDb());
    }


    /////////////////////////////////////////////////////
    // Getters to getListForDataBase()
    ////////////////////////////////////////////////////
    //-----Begin-----------------------------------------
    @NonNull
    private Callable<List<UserView>> getListCallable() {
        return () -> {
            List<UserView> record = manager.getListUsers();
            return record;
        };
    }


    @NonNull
    private Subscriber<List<UserView>> getSubFromDb() {
        return new Subscriber<List<UserView>>() {
            @Override
            public void onSubscribe(Subscription s) {
                s.request(Long.MAX_VALUE);
            }

            @Override
            public void onNext(List<UserView> record) {
                PresenterListUsers.this.setToRecicleViewElement(record);
            }

            @Override
            public void onError(Throwable t) {
                showLog("Error set to view");
                t.printStackTrace();
            }

            @Override
            public void onComplete() {
                showLog("onComplete set to view");
            }
        };
    }
    //-----End-----------------------------------------


    ////////////////////////////////////////////////////
    // Action from view
    ////////////////////////////////////////////////////
    //-----Begin-----------------------------------------

    public void ifPressThenDataBaseUpdate(String id) {
        manager.pressList(id);
    }

    //-----End-----------------------------------------


    ////////////////////////////////////////////////////
    // Action to view
    ////////////////////////////////////////////////////
    //-----Begin-----------------------------------------
    private void setToRecicleViewElement(List<UserView> record) {
        getViewState().initRecycleView(record);
    }
    //-----End-----------------------------------------


    ////////////////////////////////////////////////////
    // For show
    ////////////////////////////////////////////////////
    @SuppressLint("LongLogTag")
    private void showLog(String show) {
        Log.d(TAG, show);
    }

}
